import React from 'react'
import './Fragebogen.css';

const Produkt = ({text, icon, name}) => {
    
    return (
      <div className="Fragebogen">
    
          <p>{text}</p>
          <img src={icon} alt=""/>
          <button>{name}</button>
          
      </div>
    );
  }
  
  export default Produkt;
  