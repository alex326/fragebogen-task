import React, { useState, useEffect } from "react";
import './Fragebogen.css';
import Produkt from './produkt';

const Fragebogen = () => {
  const [data, setData] = useState([]);

  async function fetchData(url) {
    const res = await fetch(url);
    res
      .json()
      .then(res => setData(res))
  }

  useEffect(() => {
    fetchData("https://services.brandl-nutrition.de:30001/recipes/goals/strength/products/");
  },[]);


if(data.length === 0){
  return null;
}
console.log(data)

  return (
    <div className="Fragebogen">
      {data.map(x => {
        return (
        <Produkt text={x.text} icon={x.icon} name={x.name} key={x.key}/>
          )
        
      })
      }

      
    </div>
  );
}

export default Fragebogen;
